namespace MisskeyEntity {
  export type Field = {
    name: string
    value: string
  }
}
