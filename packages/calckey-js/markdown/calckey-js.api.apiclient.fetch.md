<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [api](./calckey-js.api.md) &gt; [APIClient](./calckey-js.api.apiclient.md) &gt; [fetch](./calckey-js.api.apiclient.fetch.md)

## api.APIClient.fetch property

**Signature:**

```typescript
fetch: FetchLike;
```
